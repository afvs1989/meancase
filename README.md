# MeanCaseFullstack
# Contents
* [Prerequisites](#prerequisites)
* [Usage](#usage)
* [Initial Setup](#initial-setup)

### Prerequisites
* [MongoDB](https://www.mongodb.org/)
* [NodeJs](https://nodejs.org/en/)
* [Git](https://git-scm.com/)

##### Node Packages
* npm install -g gulp
* npm install -g bower

## Usage-dev
1. Clone repository
2. Install packages -
`npm install`
`bower install`
3. Initialize Server with Gulp -
`gulp server`
`gulp watchFront`
4. Now you can run the App on the port `http://localhost:4000/`

## Usage-Production
`npm start`

## Initial Setup

**Project Settings**
* Project name
* User login Authorization  
the user created in this part will came with a root rol

**Navigation Template**  
* You can select you favorite template, but you can change o upload it inside the RootNavBar

**Once you have finish the initial setup now you are ready to work on your project**